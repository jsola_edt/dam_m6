
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author jsola
 */
public class PoliticBD {
    
    public void insereix (Politic p) {
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {            
            session.beginTransaction();
            session.save(p);
            session.getTransaction().commit();

        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }    
    
    public List<Politic> selecciona () {
        
        List<Politic> llistaPolitics = new ArrayList<>();
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session= factory.openSession();
        
        try {            
            session.beginTransaction();
            Query query= session.createQuery("from Politic");
            List lstPersones= query.list();

            for (int i=0; i<lstPersones.size(); i++) {
                Politic p= (Politic) lstPersones.get(i);
                llistaPolitics.add(p);
            }         
            session.getTransaction().commit();
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
        return llistaPolitics;
    }
        
}
