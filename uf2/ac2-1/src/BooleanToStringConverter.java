
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jsola
 */
@Converter
public class BooleanToStringConverter implements AttributeConverter<Boolean, String> {

    @Override
    public String convertToDatabaseColumn(Boolean value) {        
        return (value == null || !value) ? "N" : "S";            
        }    

    @Override
    public Boolean convertToEntityAttribute(String value) {
        return "S".equals(value);
        }
    }
