/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uf3_ac1_1;

import java.util.List;

/**
 *
 * @author Jordi
 */
public class Uf3_Ac1_1 {

    public static void exercici1() {
        Assignatura a1 = new Assignatura("Accés a dades",5);
        AssignaturaMongo.insereix(a1);
        Assignatura a2 = new Assignatura("Sistemes de gestió empresarial",3);
        AssignaturaMongo.insereix(a2);
    }
    
    public static void exercici2() {
        List<Assignatura> llistaAssig = AssignaturaMongo.llegeix(0);
        for (Assignatura assig : llistaAssig) {
            System.out.println(assig);
        }        
    }
    
    public static void exercici3() {
        AssignaturaMongo.elimina();
    }
    
    public static void main(String[] args) {
        exercici2();
    }
    
}
