/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uf3_ac1_1;

/**
 *
 * @author Jordi
 */
public class Assignatura {
    String nom;
    int hores_setmana;
    
    public Assignatura () {
        
        
    }
    public Assignatura (String nom, int hores_setmana) {
        this.nom = nom;
        this.hores_setmana = hores_setmana;
    }
    
    @Override
    public String toString() {
        String retorna = "* nom: "+this.nom+", hores_setmana: "+this.hores_setmana;
        return retorna;
    }
    
}
