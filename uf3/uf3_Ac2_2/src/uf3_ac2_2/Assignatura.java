/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uf3_ac2_2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jordi
 */
public class Assignatura {
    String nom;
    int hores_setmana;
    List<Alumne> alumnes_matriculats;
    
    public Assignatura () {
        alumnes_matriculats = new ArrayList<>();
    }
    
    public Assignatura (String nom, int hores_setmana) {
        this.nom = nom;
        this.hores_setmana = hores_setmana;
        alumnes_matriculats = new ArrayList<>();
    }
    
    @Override
    public String toString() {
        String retorna = "* nom: "+this.nom+", hores_setmana: "+this.hores_setmana+"\n";
        retorna +="-- LLISTA D'ALUMNES MATRICULATS --\n";
        for (Alumne al : alumnes_matriculats) {
            retorna += al + "\n";
        }
        return retorna;
    }
    
}
