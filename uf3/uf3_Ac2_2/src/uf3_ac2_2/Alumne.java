/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uf3_ac2_2;

/**
 *
 * @author Jordi
 */
public class Alumne {
    
    String dni;
    String nom;
    
    public Alumne (String dni, String nom) {
        this.dni = dni;
        this.nom = nom;
    }
    
    @Override
    public String toString() {
        String retorna = "* nom: "+this.nom+", dni: "+this.dni+"\n";
        return retorna;
    }
}
