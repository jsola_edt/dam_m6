/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uf3_ac2_2;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.result.DeleteResult;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bson.Document;

/**
 *
 * @author Jordi
 */
public class AssignaturaMongo {
    
  public static void insereix(Assignatura assig) {
        try(MongoClient mongoClient= MongoClients.create();)
        { 
            MongoDatabase database= mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection= database.getCollection("assignatures");
            Document doc= new Document("nom",assig.nom).
                    append("hores_setmana", assig.hores_setmana);
            List<Document> arrayAlumnes = new ArrayList<>();
            for (Alumne alu : assig.alumnes_matriculats) {
             arrayAlumnes.add(new Document("nom", alu.nom).append("dni", alu.dni));                      
            }  
            doc.append("alumnes_matriculats",arrayAlumnes);
            collection.insertOne(doc);
        }
     }
     
     public static void elimina() {
           try(MongoClient mongoClient= MongoClients.create();)
        { 
            MongoDatabase database= mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection= database.getCollection("assignatures");
            DeleteResult deleteResult = collection.deleteMany(new Document());
            System.out.println("Documents esborrats: "+deleteResult.getDeletedCount());            
        }

     }
     
     public static List<Assignatura> llegeix(int alumnes_max) {
        List<Assignatura> resultat = new ArrayList<>();
        try(MongoClient mongoClient= MongoClients.create();)
        { 
            MongoDatabase database= mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection= database.getCollection("assignatures");
            try (MongoCursor<Document> cursor = collection.find(where("this.alumnes_matriculats.length<="+alumnes_max)).iterator()) {
                    while (cursor.hasNext()) {
                    Assignatura assig = new Assignatura();
                    Document d = cursor.next();
                    assig.nom = d.getString("nom");
                    assig.hores_setmana = d.getInteger("hores_setmana");
                    List<Document> llistaAlumnes = d.getList("alumnes_matriculats", Document.class);          
                    for (Document subdoc : llistaAlumnes) {
                        Alumne alu = new Alumne(subdoc.getString("dni"),subdoc.getString("nom"));
                        assig.alumnes_matriculats.add(alu);
                    }
                    resultat.add(assig);
                }
            }
        }
        return resultat;        
     }
     
     
     
}
     

