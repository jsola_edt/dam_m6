/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uf3_ac2_2;

import java.util.List;

/**
 *
 * @author Jordi
 */
public class Uf3_Ac2_2 {

    public static void exercici1() {
        Alumne alu1 = new Alumne ("12345678X","Pepito");
        Alumne alu2 = new Alumne ("87654321Z","Fulanito");
        Alumne alu3 = new Alumne ("13243546P","Menganito");
        
        Assignatura a1 = new Assignatura("Accés a dades",5);
        a1.alumnes_matriculats.add(alu1);
        a1.alumnes_matriculats.add(alu2);        
        AssignaturaMongo.insereix(a1);
        
        Assignatura a2 = new Assignatura("Sistemes de gestió empresarial",3);
        a2.alumnes_matriculats.add(alu2);
        a2.alumnes_matriculats.add(alu3);
        AssignaturaMongo.insereix(a2);
    }
    
    public static void exercici2() {
        List<Assignatura> llistaAssig = AssignaturaMongo.llegeix(0);
        for (Assignatura assig : llistaAssig) {
            System.out.println(assig);
        }        
    }
    
    public static void exercici3() {
        List<Assignatura> llistaAssig = AssignaturaMongo.llegeix(2);
        for (Assignatura assig : llistaAssig) {
            System.out.println(assig);
        }        
    }
    
    public static void main(String[] args) {
        exercici3   ();
    }
    
}
