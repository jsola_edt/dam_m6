/**
 *
 * @author Jordi
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Properties;
import java.util.Scanner;

public class Ac2_5 {

    /**
     * @param args the command line arguments
     */
    
    public static Hospital generaHospital() {
        Calendar cal = Calendar.getInstance();
        
        cal.set(1965, 3, 23);
        Metge m1 = new Metge("Maria",25, cal.getTime());
        
        cal.set(1980, 8, 2);
        Metge m2 = new Metge("Joan",13, cal.getTime());
        
        Hospital hospital = new Hospital("Hospital Clínic","C. de Villaroel 170",true);
        hospital.llistaMetges.add(m1);
        hospital.llistaMetges.add(m2);

        return hospital;
    }
    
    public static int menu() {
        System.out.println("Seleccioneu una opció:"
                + "\n 0.- Sortir"
                + "\n 1.- Generar i desar hospital"
                + "\n 2.- Llegir i pintar hospital");
        
        Scanner teclat = new Scanner(System.in);      
        return teclat.nextInt(); // TODO comprovar que el número és correcte
    }
    
    public static String obteRuta() throws FileNotFoundException, IOException {
        String ruta = "";
        try {
            InputStream input = new FileInputStream("src/config.properties");
            Properties p = new Properties();
            p.load(input);
            ruta = p.getProperty("rutaFitxer");            
        } catch (Exception ex) {
            System.out.println("Error llegint el fitxer de propietats");
            throw ex;
        }
        // TODO: feu que llegeixi la ruta d'un fitxer de Properties. 
        // Nom de la propietat: "rutaFitxer".
        return ruta;
    }
               
    public static void main(String[] args) throws IOException {
        int opcio = menu();
        Hospital hospital;
        String ruta = obteRuta();        
        switch (opcio) {
            case 1:
                hospital = generaHospital();
                hospital.desa(ruta);
                break;
            case 2:
                // fem la lectura del fitxer a través del constructor
                hospital = new Hospital(new File(ruta));
                // imprimim l'hospital. Això funciona perquè hem sobreescrit
                // el mètode toString a la classe Hospital
                System.out.println(hospital);
                break;
     }
    }
}
