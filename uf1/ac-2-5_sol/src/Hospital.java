
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jordi
 */
public class Hospital {
    String nom;
    String adreca;
    boolean esPublic;
    List<Metge> llistaMetges = new ArrayList<>();
        
    // Aquest constructor rep un File i fa la lectura de l'hospital del fitxer
    public Hospital(File fitxerLectura) throws IOException {
        try (DataInputStream entrada = new DataInputStream(new
        BufferedInputStream(new FileInputStream(fitxerLectura)));) {
            this.nom = entrada.readUTF();
            this.adreca = entrada.readUTF();
            this.esPublic = entrada.readBoolean();
            int numMetges = entrada.readInt();
            for (int i=0; i < numMetges; i++) {
                Metge m = new Metge(entrada);
                llistaMetges.add(m);
            }
        }
    }

    public Hospital(String nom, String adreca, boolean esPublic) {
        this.nom = nom;
        this.adreca = adreca;
        this.esPublic = esPublic;
    }
    
    public void desa(String ruta) throws IOException {
        // TODO: implementeu aquí el mètode de desat     
        try (DataOutputStream sortida = new DataOutputStream(new
        BufferedOutputStream(new FileOutputStream(ruta)));) {
            sortida.writeUTF(this.nom);
            sortida.writeUTF(this.adreca);
            sortida.writeBoolean(this.esPublic);
            sortida.writeInt(llistaMetges.size());
            for (Metge m : this.llistaMetges) {
                m.desa(sortida);
            }
        }
    }
    
    // Sobreescrivim el mètode toString per poder pintar per pantalla hospitals
    @Override
    public String toString() { 
        String retorna = "==== Dades de l'hospital ====\n";
        retorna+="Nom: "+this.nom+"\n";
        retorna+="Adreça: "+this.adreca+"\n";
        retorna+="És public?: "+ (this.esPublic ? "Sí" : "No") +"\n";
        retorna+="*** Llista de metges de l'hospital***\n";
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        for (Metge m : llistaMetges) {
            retorna+="Nom del metge: "+m.nom+"\n";
            retorna+="Número de consulta: "+m.numConsulta+"\n";            
            retorna+="Data de naixement: "+df.format(m.dataNaixement)+"\n";
            retorna+="--------\n";
        }
        return retorna;
    } 
    
}
