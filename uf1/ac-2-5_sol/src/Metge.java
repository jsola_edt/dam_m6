
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jordi
 */
public class Metge {
    String nom;
    int numConsulta;
    Date dataNaixement;
    
    public Metge(String nom, int numConsulta, Date dataNaixement) {
        this.nom = nom;
        this.numConsulta = numConsulta;
        this.dataNaixement = dataNaixement;
    }
    
    public Metge (DataInputStream entrada) throws IOException {
        this.nom = entrada.readUTF();
        this.numConsulta = entrada.readInt();
        this.dataNaixement = new Date(entrada.readLong());                
    }
    
     public void desa(DataOutputStream sortida) throws IOException {
        sortida.writeUTF(this.nom);
        sortida.writeInt(this.numConsulta);
        sortida.writeLong(this.dataNaixement.getTime());
    }
}
