/**
 *
 * @author Jordi
 */
public class Ac3_1 {

    public static void main(String[] args) {
        Llibreria casa = new Llibreria("Casa del llibre", "Vic");
        Llibre llib1 = new Llibre("El quadern gris","Josep Pla");
        Llibre llib2 = new Llibre("El perquè de tot plegat","Quim Monzó");
        casa.llibres.add(llib1);
        casa.llibres.add(llib2);
    }
    
}
