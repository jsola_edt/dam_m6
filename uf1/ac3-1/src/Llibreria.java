
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jordi
 */
public class Llibreria {
    String nom;
    String poblacio;
    List<Llibre> llibres = new ArrayList<>();

    public Llibreria(String nom, String poblacio) {
        this.nom = nom;
        this.poblacio = poblacio;
    }
    
    public void imprimir() {
        System.out.println("===== DADES DE LA LLIBRERIA ====");
        System.out.println("Nom: "+this.nom);
        System.out.println("Població: "+this.poblacio);
        System.out.println("Llibres:");
        for (Llibre llibre : this.llibres) {
            llibre.imprimir();
        }
        System.out.println("================================");
    }
}
