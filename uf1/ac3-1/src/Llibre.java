/**
 *
 * @author Jordi
 */
public class Llibre {
    String autor;
    String titol;
    
    public Llibre(String autor, String titol) {
        this.autor = autor;
        this.titol = titol;
    }
   
    public void imprimir() {
        System.out.println("Títol: "+this.titol);
        System.out.println("Autor: "+this.autor);
        System.out.println("---");
    }
}
