
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jordi
 */
public class Treballador implements Serializable {
    String nif;
    String nom;
    int sou;
    
    public void desar_v1(String ruta) throws IOException {
        ObjectOutputStream sortida = null;
        try {
            sortida = new ObjectOutputStream(new FileOutputStream(ruta));
            sortida.writeObject(this); // guardem el propi treballador
        }
        finally {
            sortida.close();
        }
    }
    
    public void llegir_v1(String ruta) throws IOException, ClassNotFoundException {
        ObjectInputStream entrada = null;
        try {
            entrada = new ObjectInputStream(new FileInputStream(ruta));
            Treballador llegit = (Treballador) entrada.readObject(); // guardem el propi treballador
            this.nif = llegit.nif;
            this.nom = llegit.nom;
            this.sou = llegit.sou;
        }
        finally {
            entrada.close();
        }
    }
    
    public void desar_v3(String ruta) throws IOException {
        DataOutputStream sortida = new DataOutputStream(new
        BufferedOutputStream(new FileOutputStream(ruta)));
        try {
            sortida.writeUTF(this.nif);
            sortida.writeUTF(this.nom);
            sortida.writeInt(this.sou);
        }
        finally {
            sortida.close();
        }
    }
    
    public void llegir_v3(String ruta) throws IOException, ClassNotFoundException {
        DataInputStream entrada = new DataInputStream(new
        BufferedInputStream(new FileInputStream(ruta)));
        try {
            this.nif = entrada.readUTF();
            this.nom = entrada.readUTF();
            this.sou = entrada.readInt();
        }
        finally {
            entrada.close();
        }
    }
    
    public void imprimir() {  
         System.out.println("DNI: " + this.nif);
         System.out.println("Nom: " + this.nom);
         System.out.println("Sou: " + this.sou);    
    }
    
}
