
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jordi
 */
public class Ac2_3 {

     public static int menu() {
        System.out.println("Seleccioneu una opció:"
                + "\n 0.- Sortir"
                + "\n 1.- Emmagatzemar treballador"
                + "\n 2.- Recuperar treballador");
        
        Scanner teclat = new Scanner(System.in);      
        return teclat.nextInt(); // TODO comprovar que el número és correcte
    }
     
     public static Treballador demanarTreballador () {
         Treballador t = new Treballador();
         Scanner teclat = new Scanner(System.in);      
         System.out.println("Introduiu DNI:");
         t.nif = teclat.nextLine();
         System.out.println("Introduiu nom:");
         t.nom = teclat.nextLine();
         System.out.println("Introduiu sou:");
         t.sou = teclat.nextInt();
         
         return t;
     }
    
public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        // TODO: gestionar l'excepció de fitxer no trobat o IOException
        int opcio = menu();
        Treballador t;
        String ruta = "/Users/Jordi/ac2-3.txt";
        switch (opcio) {
            case 1:
                t = demanarTreballador();
                t.desar_v3(ruta);
                break;
            case 2:
                t = new Treballador();
                t.llegir_v3(ruta);
                t.imprimir();
                break;
       
        }
    }
    
}
