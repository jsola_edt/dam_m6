
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jordi
 */
public class Hospital {
    String nom;
    String adreca;
    boolean esPublic;
    List<Metge> llistaMetges = new ArrayList<>();
        
    // Aquest constructor rep un File i fa la lectura de l'hospital del fitxer
    public Hospital(File fitxerLectura) {
        // TODO: implementar la lectura
    }

    public Hospital(String nom, String adreca, boolean esPublic) {
        this.nom = nom;
        this.adreca = adreca;
        this.esPublic = esPublic;
    }
    
    public void desa(String ruta) {
        // TODO: implementeu aquí el mètode de desat
    }
    
    // Sobreescrivim el mètode toString per poder pintar per pantalla hospitals
    @Override
    public String toString() { 
        String retorna = "==== Dades de l'hospital ====\n";
        retorna+="Nom: "+this.nom+"\n";
        retorna+="Adreça: "+this.adreca+"\n";
        retorna+="És public?: "+ (this.esPublic ? "Sí" : "No") +"\n";
        retorna+="*** Llista de metges de l'hospital***\n";
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        for (Metge m : llistaMetges) {
            retorna+="Nom del metge: "+m.nom+"\n";
            retorna+="Número de consulta: "+m.numConsulta+"\n";            
            retorna+="Data de naixement: "+df.format(m.dataNaixement)+"\n";
            retorna+="--------\n";
        }
        return retorna;
    } 
    
}
